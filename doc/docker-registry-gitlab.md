# Gitlab docker registry integration

We now use gitlab's container registry to manage private docker images built
from this repo. For private groups/projects, even the pull access requires
authentication. The need and way to create either a personal access token or a
deploy token is very well described in gitlab at project's Registry page.

### Authentication for push
To be able to push new images to the registry, the user needs to authenticate.
First, [create a personal access
token](https://gitlab.com/help/user/profile/personal_access_tokens.md) (with
`api` scope). Then log in to docker:
```
docker login registry.gitlab.com -u <username> -p <token>
```
See [Gitlab
documentation](https://gitlab.com/help/user/project/container_registry) for
details.

### Authentication in CI jobs
1. Generate a [deploy
token](https://gitlab.com/help/user/project/deploy_tokens/index.md) in the
registry's gitlab repository, with the `read_registry` scope. Use one deploy
token per project (or group) to enable fine-grained revocation (and also more
practically because the token value cannot be revealed later). 2. In the project
where the CI requires an image, edit CI/CD Environment Variables. Add
`DOCKER_AUTH_CONFIG` with value such as:
```
{"auths":{"registry.gitlab.com":{"auth":"..."}}}
```
where the value of auth is the output of `echo -n "TOKEN_LOGIN:TOKEN_PASSWORD" |
base64`, where `TOKEN_LOGIN` and `TOKEN_PASSWORD` are the credentials shown when
creating the deploy token in step 1. The `-n` argument to echo is important and
means that a newline will not be included in the text – be sure to remember it
or it will not work!
