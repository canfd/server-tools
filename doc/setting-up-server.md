# Setting up new server

The basics
 * install etckeeper, mc, tmux
 * disable SSH password login
 * set up users, copy default config
 * install docker
 * install docker-compose (just download the binary from github)
 * add stretch-backports repo, install git-lfs

Bash completion:
 * install bash-completion
 * in `/etc/bash.bashrc`, uncomment the code block "enable bash completion in interactive shells"

SW Images:
 * copy Xilinx / Quartus images to the VM
 * install squashfuse (requires FUSE feature)
   * install build-essential autoconf pkg-config libtool libz-dev libfuse-dev
   * git clone & build & install
   * run ldconfig
 * set up automounting via /etc/fstab

Docker:
 * clone the SW tools repository
 * bring up all the stack via `docker-compose up -d`. From now on, they should be started automatically.
 * set up a cron job for the update script
 * set up cron job for nginx stack health check

MTA:
 * install msmtp, msmtp-mta
 * set up `/etc/msmtprc`
 * configure `/etc/aliases.src`
 * set up git pre-commit hook: add
```
/etc/expand-aliases.py </etc/aliases.src >/etc/aliases
git add /etc/aliases
```

Unattended upgrades:
 * install unattended-upgrades needrestart bsd-mailx apt-listchanges
 * modify `/etc/apt/apt.conf.d/50unattended-upgrades`

## Xilinx Image

* install Vivado & SDK
* modify all paths in all settings.sh files
* copy microzed board files {SDK,Vivado}/2018.2/data/boards/board_files/microzed_70[12]0
* squash

## Quartus Image

Install Quartus lite and squash. No changes have to be made. Even modelsim now
works out of the box.

## Prometheus

* install prometheus
* modify listen address of prometheus and prometheus-node-exporter to localhost only
* set up gitlab-runner exporter (in repo), add it to prometheus config


## Various odd tidbits

### Setting number of Vivado synthesis jobs in CI

The `build.tcl` scripts looks for environment variable `VIVADO_NJOBS` and
defaults to `` `nproc` `` if it is not set. The `VIVADO_NJOBS` variable is set
in gitlab's CI Environment variables.
