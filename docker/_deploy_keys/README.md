# SSH Deploy keys

This is a store of SSH keys used to enable push to repo from CI.
Used in [zynq-top](https://gitlab.fel.cvut.cz/canbus/zynq/zynq-can-sja1000-top) to push to `autobuild_*`
branches.

The public key should be registered in gitlab in Settings - Repository - Deploy keys.
The private key may be stored as:
1. On runner, in a docker volume, which is automatically mounted in CI containers (current way).
   * Specify the volume in gitlab-runner's `config.toml` in `volumes`. Mount it to `/depkey`.
   * Be sure to include SSH `config` with `StrictHostKeyChecking no`.
2. In gitlab Secret variables. This is slightly simpler for maintenance, but slightly less secure.

**NOTE**: The keys themselves are not versioned in git (for obvious reasons).
