# gitlab-runner image

The runner must register with the projects whose jobs it is supposed to run.
Ideally, just manually register the runner with a group on gitlab and then just
specify "use group runner" for each individual project.

## Configuration

The runner configuration is stored in `/etc/gitlab-runner/config.toml` inside the container.
As projects may be dynamically registered, the directory `/etc/gitlab-runner` is
a docker volume and persists between container restarts/updates.

### Making Vivado/Quartus/Modelsim available to the runner

The big proprietary SW is not embedded in docker images, it is just mounted into the
containers running the actual job as a bind mount. This has to be specified in the
`config.toml` for each registered project in the `volumes` field. See the template
in `config.toml.template`. Currently this has to be done manually.

# Notes

The container uses the host's docker daemon to launch job containers (`/var/run/docker.sock` bind-mounted).
From security point of view, it is thus of little value to have this in container.
