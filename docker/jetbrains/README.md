# Jetbrains SW docker stack

This was mainly for trying out the SW and is not used in production.

Running:
 * create the data structure by running `./init`
 * select the SW versions in `docker-compose.yml`
 * `docker-compose up -d`

May all the RAMs in the multiverse be with you.
