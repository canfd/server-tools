# Documentation builder image

Packages:
 * latex
 * lyx
 * imagemagick

The supported usage is following:
```
$ lyx --export-to latex XY.tex XY.lyx
$ sed -rie '/{inputenc}/d' XY.tex
$ iconv -f latin2 -t utf8 <XY.tex >XY.tex-1 && mv XY.tex-1 XY.tex
$ xelatex XY
$ xelatex XY
$ xelatex XY
```

We have to use xelatex, because otherwise the fonts are ugly. And to use xelatex,
we must remove the `inputenc` specification from the tex file.

The image uses lyx from stretch-backports, which is new enough.
