#!/bin/bash

set -eu

GCC_VER="$1"

wget -c https://ftp.gnu.org/gnu/gcc/gcc-${GCC_VER}/gcc-${GCC_VER}.tar.gz
[ -d gcc-${GCC_VER} ]   || tar xf gcc-${GCC_VER}.tar.gz

(cd gcc-${GCC_VER} && patch -p1 </build/gcc-isl.patch)

git clone --depth=1 https://github.com/ghdl/ghdl.git

cd ghdl
mkdir build; cd build
../configure --with-gcc=../../gcc-${GCC_VER} --prefix=/opt/ghdl

make copy-sources
mkdir gcc-objs; cd gcc-objs
../../../gcc-${GCC_VER}/configure --prefix=/opt/ghdl --enable-languages=c,vhdl \
    --disable-bootstrap --enable-lto --disable-multilib --disable-libssp \
    --disable-libgomp --disable-libquadmath --enable-default-pie
make -j`nproc`
make install -j`nproc`
cd ..
make ghdllib -j`nproc`
make install -j`nproc`
cd /
rm -Rf /build

strip /opt/ghdl/bin/* /opt/ghdl/libexec/gcc/*-pc-linux-gnu/*/{cc1,collect2,ghdl1,lto1,lto-wrapper}
