#!/bin/bash

git clone https://github.com/nickg/nvc.git
cd nvc

./autogen.sh
mkdir build
cd build

../configure --prefix=/opt/nvc

make -j`nproc`
make install

cd ..
rm -rf build