#!/bin/bash

set -eu

GCC_VER="$1"
GHDL_VER="$2"

wget -c https://ftp.gnu.org/gnu/gcc/gcc-${GCC_VER}/gcc-${GCC_VER}.tar.gz https://github.com/ghdl/ghdl/archive/v${GHDL_VER}.tar.gz
[ -d gcc-${GCC_VER} ]   || tar xf gcc-${GCC_VER}.tar.gz
[ -d ghdl-${GHDL_VER} ] || tar xf v${GHDL_VER}.tar.gz

cd ghdl-${GHDL_VER}
mkdir build; cd build
../configure --with-gcc=../../gcc-${GCC_VER} --prefix=/opt/ghdl

make copy-sources
mkdir gcc-objs; cd gcc-objs
../../../gcc-${GCC_VER}/configure --prefix=/opt/ghdl --enable-languages=c,vhdl \
    --disable-bootstrap --enable-lto --disable-multilib --disable-libssp \
    --disable-libgomp --disable-libquadmath
make -j`nproc`
make install
cd ..
make ghdllib
make install
cd /
rm -Rf /build

strip /opt/ghdl/bin/* /opt/ghdl/libexec/gcc/*-pc-linux-gnu/*/{cc1,collect2,ghdl1,lto1,lto-wrapper}
