# Simulator image

This image contains:
 * ghdl (with gcc backend, capable of code coverage)
 * testfw dependencies: VUnit, click, pyyaml, jinja2, json2html, lcov
 * gcc/g++
 * NVC simulator

## Simulator with gui

:gui

 * gtkwave

## Variants

Only the `upstream` variant is used and supported. `custom` is deprecated and kept only for archive purposes.
